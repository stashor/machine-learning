import java.lang.reflect.Array;
import java.util.*;
import java.io.*;

/*
    Класс, отвечающий за чтение файлов
*/


public class Reader {
    public class InputFile {
        String fileName; // заголовок файла
        String fileStrings; // тело файла

        public InputFile() {
            this.fileName = "";
            this.fileStrings = "";
        }

        @Override
        public String toString() {
            System.out.println(this.fileName);
            System.out.println(this.fileStrings);
            return super.toString();
        }
    }

    // считываем все файлы из одной части, на выходе массив объектов InputFile
    public ArrayList<InputFile> readPart(File folder) {
        try {
            File[] files = folder.listFiles();  // папка откуда считываем
            ArrayList<InputFile> inputFiles = new ArrayList<>();

            int i = 0;
            for (File entry : files) {
                inputFiles.add(new InputFile());
                Scanner scanner = new Scanner(entry);
                inputFiles.get(i).fileName = entry.getPath().substring(64);  // получаем заголовок файла
                while (scanner.hasNext()) {
                    inputFiles.get(i).fileStrings += scanner.nextLine() + "\n";  // считываем файл в строку
                }
                i++;
            }
            return inputFiles;
        } catch (IOException e) {
            System.out.println(e.getStackTrace());
            return new ArrayList<>();
        }
    }

    public void printFiles(InputFile[] inputFiles) {
        for (InputFile ip: inputFiles) {
            System.out.println(ip);
        }
    }
}
