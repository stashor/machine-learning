import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/*
    Класс, в котором хранится один файл
*/

public class DataSet {
    boolean type; // тип сообщения (true = нормальное, false = спам)
    HashMap<Integer, Integer> words; // все слова в файле и их количество
    HashMap<Integer, Integer> headerWords; // все слова в загаловке и их количество
    HashMap<Integer, Integer> bodyWords; // все слова в теле и их количество

    public DataSet(Reader.InputFile inputFile) { // конструктор датасета из одного файла
        this.type = inputFile.fileName.indexOf("legit") != -1 ? true : false;  // проверка спам или нет

        Scanner scanner = new Scanner(inputFile.fileStrings);
        this.headerWords = new HashMap<>();
        this.bodyWords = new HashMap<>();
        this.words = new HashMap<>();
        int key;
        Scanner scannerHeader;

        String header = scanner.nextLine();
        if (header.length() >= 10) {
            scannerHeader = new Scanner(header.substring(10)); // сканер заголовка
        } else {
            scannerHeader = new Scanner(""); // сканер заголовка
        }

        while (scannerHeader.hasNext()) { // обрабатываем заголовок
            key = scannerHeader.nextInt();
            headerWords.put(key, (headerWords.containsKey(key) ? headerWords.get(key) + 1 : 1));
            words.put(key, (words.containsKey(key) ? words.get(key) + 1 : 1));
        }
        scanner.nextLine();

        Scanner scannerBody = new Scanner(scanner.nextLine()); // сканер тела
        while (scannerBody.hasNext()) { // обрабатываем тело
            key = scannerBody.nextInt();
            this.bodyWords.put(key, (this.bodyWords.containsKey(key) ? this.bodyWords.get(key) + 1 : 1));
            this.words.put(key, (this.bodyWords.containsKey(key) ? this.bodyWords.get(key) + 1 : 1));
        }
    }

    @Override
    public String toString() {
        System.out.println(this.type);
        System.out.println(this.headerWords);
        System.out.println(this.bodyWords);
        System.out.println(this.words);
        return super.toString();
    }
}