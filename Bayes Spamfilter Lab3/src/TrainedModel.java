import java.util.HashMap;
import java.util.Map;

/*
    Класс обученной модели
*/

public class TrainedModel {
    public Map<Integer, Double> headerWordsSpamicity;
    public Map<Integer, Double> bodyWordsSpamicity;
    public Map<Integer, Double> allWordsSpamicity;

    public TrainedModel(Map<Integer, Double> headerWordsSpamicity,
                        Map<Integer, Double> bodyWordsSpamicity,
                        Map<Integer, Double> allWordsSpamicity) {
        this.headerWordsSpamicity = new HashMap<>(headerWordsSpamicity);
        this.bodyWordsSpamicity = new HashMap<>(bodyWordsSpamicity);
        this.allWordsSpamicity = new HashMap<>(allWordsSpamicity);
    }

    @Override
    public String toString() {
        System.out.println(headerWordsSpamicity);
        System.out.println(bodyWordsSpamicity);
        System.out.println(allWordsSpamicity);
        return super.toString();
    }
}
