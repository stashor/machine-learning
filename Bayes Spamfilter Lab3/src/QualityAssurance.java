import java.util.ArrayList;
import java.util.HashMap;

/*
    Оценка качества работы алгоритма
 */

public class QualityAssurance {
    public void Accuracy(ArrayList<Boolean[]> answers) {
        int rightAnswers = 0;

        for (Boolean[] answer: answers) {
            rightAnswers += (answer[0] == answer[1] ? 1 : 0);
        }
        System.out.println("Accuracy = " + (double) rightAnswers / answers.size());
    }

    public void F_measure(ArrayList<Boolean[]> answers) {
        int TP = 0;
        int FP = 0;
        int FN = 0;
        int TN = 0;

        for (Boolean[] answer: answers) {
            if (answer[0] == true && answer[1] == true) {
                TP++;
            } else if (answer[0] == true && answer[1] == false) {
                FN++;
            } else if (answer[0] == false && answer[1] == true) {
                FP++;
            } else if (answer[0] == false && answer[1] == false) {
                TN++;
            }
        }

        double precision = (double) TP / (TP + FN);
        double recall = (double) TP / (TP + FN);

        double F = 2 * (precision * recall) / (precision + recall);
        System.out.println("Precision = " + precision);
        System.out.println("Recall = " + recall);
        System.out.println("F-measure = " + F);
    }
}
