/*
    Байесовский спам фильтр
*/

public class BayesSpamFilter {
    public boolean predict(DataSet dataSet, TrainedModel trainedModel) {
        double aHeader = 1;
        double bHeader = 1;

        double aBody = 1;
        double bBody = 1;

        for (int i: dataSet.headerWords.keySet()) {
            if (trainedModel.headerWordsSpamicity.containsKey(i)) {
                if (trainedModel.headerWordsSpamicity.get(i) > 0.01 && trainedModel.headerWordsSpamicity.get(i) < 0.99) {
                    aHeader = aHeader * trainedModel.headerWordsSpamicity.get(i);
                    bHeader = bHeader * (1 - trainedModel.headerWordsSpamicity.get(i));
                }
            }
        }
        double headerSpamProbability = aHeader / (aHeader + bHeader);

        for (int i: dataSet.bodyWords.keySet()) {
            if (trainedModel.bodyWordsSpamicity.containsKey(i)) {
                if (trainedModel.bodyWordsSpamicity.get(i) > 0.01 && trainedModel.bodyWordsSpamicity.get(i) < 0.99) {
                    aBody = aBody * trainedModel.bodyWordsSpamicity.get(i);
                    bBody = bBody * (1 - trainedModel.bodyWordsSpamicity.get(i));
                }
            }
        }
        double bodySpamProbability = aBody / (aBody + bBody);

        double a = headerSpamProbability * bodySpamProbability;
        double b = (1 - bodySpamProbability) * (1 - headerSpamProbability);
        double spamProbability = a / (a + b);

        if (spamProbability > 0.5) {
            return false;
        } else {
            return true;
        }
    }
}
