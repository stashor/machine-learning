import java.io.File;
import java.util.*;

public class Main {
    private static final String path = "/Users/stassorohov/Desktop/MachineLearning_Lab3/Bayes/pu1/part";

    public static void main(String[] args) {
        Reader reader = new Reader();
        Teaching teaching = new Teaching();
        QualityAssurance qa = new QualityAssurance();
        ArrayList<Reader.InputFile> trainFiles = new ArrayList<>();
        ArrayList<Reader.InputFile> testFiles = new ArrayList<>();

        // Считываем файлы
        int flag = 3;
        for (int i = 1; i <= 10; i++) {
            ArrayList<Reader.InputFile> inputFiles = reader.readPart(new File(path + i));
            if (i != flag) {
                trainFiles.addAll(inputFiles);
            } else {
                testFiles.addAll(inputFiles);
            }
        }

        // Формируем тренировочный датасет
        ArrayList<DataSet> trainDataSet = new ArrayList<>();
        for (Reader.InputFile tf: trainFiles) {
            trainDataSet.add(new DataSet(tf));
        }

        // Формируем тестовый датасет
        ArrayList<DataSet> testDataSet = new ArrayList<>();
        for (Reader.InputFile tf: testFiles) {
            testDataSet.add(new DataSet(tf));
        }

        // Обучение
        TrainedModel trainedModel = teaching.teach(trainDataSet);

        // непосредственно классификатор
        BayesSpamFilter bayesSpamFilter = new BayesSpamFilter();

        ArrayList<Boolean[]> answers = new ArrayList<>();

        for (DataSet tds: testDataSet) {
            boolean ans = bayesSpamFilter.predict(tds, trainedModel);
            Boolean[] bool = new Boolean[2];
            bool[0] = tds.type;
            bool[1] = ans;
            answers.add(bool);
        }

        // оценка качества работы алгоритма
        qa.Accuracy(answers);
        qa.F_measure(answers);
    }
}
