import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*
    Обучение Байесовского классификатора
*/


public class Teaching {

    public TrainedModel teach(ArrayList<DataSet> dataSets) {
        int spamNumber = 0; // количество писем спама
        int hamNumber = 0; // количество нормальных писем
        Map<Integer, Integer> headerWordsInSpam = new HashMap<>(); // количество раз слово попало в заголовок спама
        Map<Integer, Integer> headerWordsInHam = new HashMap<>(); // количество раз слово попало в заголовок нормального
        Map<Integer, Double> headerWordsSpamicity = new HashMap<>(); // спамовость слов в заголовке

        Map<Integer, Integer> bodyWordsInSpam = new HashMap<>(); // количество раз слово попало в тело спама
        Map<Integer, Integer> bodyWordsInHam = new HashMap<>(); // количество раз слово попало в тело нормального
        Map<Integer, Double> bodyWordsSpamicity = new HashMap<>(); // спамовость слов в теле

        Map<Integer, Integer> allWordsInSpam = new HashMap<>(); // количество раз слово попало в спам
        Map<Integer, Integer> allWordsInHam = new HashMap<>(); // количество раз слово попало в нормальное
        Map<Integer, Double> allWordsSpamicity = new HashMap<>(); // спамовость слов в общем


        // Считаем для заголовков, тела и объединенно количество попаданий слова в спам или нормальное письмо
        for (DataSet ip: dataSets) {
            if (ip.type) {
                hamNumber++;  // для нормальных писем
                for (Integer i: ip.headerWords.keySet()) {
                    headerWordsInHam.put(i, (headerWordsInHam.containsKey(i) ? headerWordsInHam.get(i) + 1 : 1));
                    headerWordsInSpam.put(i, (headerWordsInSpam.containsKey(i) ? headerWordsInSpam.get(i) : 0));
                }
                for (Integer i: ip.bodyWords.keySet()) {
                    bodyWordsInHam.put(i, (bodyWordsInHam.containsKey(i) ? bodyWordsInHam.get(i) + 1 : 1));
                    bodyWordsInSpam.put(i, (bodyWordsInSpam.containsKey(i) ? bodyWordsInSpam.get(i) : 0));
                }
                for (Integer i: ip.words.keySet()) {
                    allWordsInHam.put(i, (allWordsInHam.containsKey(i) ? allWordsInHam.get(i) + 1 : 1));
                    allWordsInSpam.put(i, allWordsInSpam.containsKey(i) ? allWordsInSpam.get(i) : 0);
                }
            } else {
                spamNumber++;  // для спама
                for (Integer i: ip.headerWords.keySet()) {
                    headerWordsInSpam.put(i, (headerWordsInSpam.containsKey(i) ? headerWordsInSpam.get(i) + 1 : 1));
                    headerWordsInHam.put(i, (headerWordsInHam.containsKey(i) ? headerWordsInHam.get(i) : 0));
                }
                for (Integer i: ip.bodyWords.keySet()) {
                    bodyWordsInSpam.put(i, (bodyWordsInSpam.containsKey(i) ? bodyWordsInSpam.get(i) + 1 : 1));
                    bodyWordsInHam.put(i, (bodyWordsInHam.containsKey(i) ? bodyWordsInHam.get(i) : 0));
                }
                for (Integer i: ip.words.keySet()) {
                    allWordsInSpam.put(i, (allWordsInSpam.containsKey(i) ? allWordsInSpam.get(i) + 1 : 1));
                    allWordsInHam.put(i, (allWordsInHam.containsKey(i) ? allWordsInHam.get(i) : 0));
                }
            }
        }

        // расчитываем спамовость числа для заголовков
        for (Integer i: headerWordsInHam.keySet()) {
            double value = 0;
            value = ((double) headerWordsInSpam.get(i) / spamNumber)
                    / (((double) headerWordsInSpam.get(i) / spamNumber) + ((double) headerWordsInHam.get(i) / hamNumber));
            headerWordsSpamicity.put(i, value);
        }

        // расчитываем спамовость числа для тела
        for (Integer i: bodyWordsInHam.keySet()) {
            double value = 0;
            value = ((double) bodyWordsInSpam.get(i) / spamNumber)
                    / (((double) bodyWordsInSpam.get(i) / spamNumber) + ((double) bodyWordsInHam.get(i) / hamNumber));
            bodyWordsSpamicity.put(i, value);
        }

        // расчитываем спамовость числа для всех слов
        for (Integer i: allWordsInHam.keySet()) {
            double value = 0;
            value = ((double) allWordsInSpam.get(i) / spamNumber)
                    / (((double) allWordsInSpam.get(i) / spamNumber) + ((double) allWordsInHam.get(i) / hamNumber));
            allWordsSpamicity.put(i, value);
        }

        TrainedModel trainedModel = new TrainedModel(headerWordsSpamicity, bodyWordsSpamicity, allWordsSpamicity);
        return trainedModel;
    }
}
